<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
	ROUTE HOME
*/
Route::get('/', function(){
	return redirect('scrap');
});

Route::get('home', 'HomeController@index');

/*
	ROUTE USER
*/

/*
	ROUTE GUEST
*/

/*
	ROUTE ADMIN
*/

/*
	ROUTE SCRAP
*/
Route::get('scrap', 'ScrapController@get');
Route::post('scrap', 'ScrapController@post');

/*
	ROUTE TEST
*/
Route::get('uji', ['uses'=>'TestController@get']);

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

