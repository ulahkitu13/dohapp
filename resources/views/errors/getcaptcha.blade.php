@extends('errors.header')

@section('getcaptcha')
<div class="ui two column centered grid">
	<div class="column">
		<div class="ui error message">
			<div class="header">
				There is a problem on the process. We are sorry for the inconvenience !
			</div>
			<p>
				You may try again by clicking <a href="{{ URL::to('/') }}">Here</a>
			</p>
		</div>
	</div>
</div>
@endsection