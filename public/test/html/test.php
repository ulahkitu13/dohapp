
<!-- saved from url=(0042)http://localhost/dohinsurance/public/scrap -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="copyright" content="© 2004 Land Transport Authority">






<link rel="stylesheet" type="text/css" media="screen" href="./Transfer Fee Enquiry_files/vrl.css">  

<link rel="stylesheet" type="text/css" media="print" href="./Transfer Fee Enquiry_files/vrl.css"> 
 


<!--  WIS enabled -->
<link rel="stylesheet" type="text/css" media="screen" href="./Transfer Fee Enquiry_files/wis.css">  




<link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="./Transfer Fee Enquiry_files/small-device.css">

<title>Transfer Fee Enquiry</title>
  
<script language="JavaScript" src="./Transfer Fee Enquiry_files/commonweb.js"></script>
<script language="javascript" src="./Transfer Fee Enquiry_files/vrlCommons.js"></script>

<script language="JavaScript" type="text/javascript" src="./Transfer Fee Enquiry_files/jsbn.js"></script>
<script language="JavaScript" type="text/javascript" src="./Transfer Fee Enquiry_files/prng4.js"></script>
<script language="JavaScript" type="text/javascript" src="./Transfer Fee Enquiry_files/rng.js"></script>
<script language="JavaScript" type="text/javascript" src="./Transfer Fee Enquiry_files/rsa.js"></script>
<script language="JavaScript" type="text/javascript" src="./Transfer Fee Enquiry_files/vrlwebrsa.js"></script>


<script language="javascript" src="./Transfer Fee Enquiry_files/jquery-1.7.1.min.js"></script>
<!--  
<script language="javascript" src="/vrl/scripts/plupload/plupload.full.js"></script>
-->

<link rel="stylesheet" type="text/css" media="screen" href="./Transfer Fee Enquiry_files/fileupload.css">  




	
<script language="javascript" src="./Transfer Fee Enquiry_files/fileupload.js"></script>


<script>

var common = {};

common.log = function log(msg) {
    if (typeof console != "undefined") {
        console.log(msg);
    }
    else if (window.console && 'function' === typeof window.console.log) { //for IE
		window.console.log(msg);
	}
}


var checkersIDList =new Array("approvalUserNumber",
							  "approveId",
							  "approveUserId",
							  "authorisationId",
							  "checkId",
							  "checkerID",
							  "checkerId",
							  "supervisorID",
							  "supervisorId",
							  "usrId",
							  "waiveCheckerID"); 

function revealPage() {
	if (parent.frames && parent.frames.length && parent.frames.length > 0) {
		hideElement("main", "layoutPlsWait");
		showElement("main", "layoutBody");
		showElement("main", "layoutContent");
		showElement("main", "layoutSubContent");
		showElement("main", "layoutDescription");
		showElement("main", "layoutProgressBar");
		showElement("banner", "theMenuBlock");
		parent.frames['timer'].focus();
	} else {
		var elem1 = document.getElementById("layoutPlsWait");
		if (elem1) {
			if (elem1.style) {
				if (elem1.style.display != null) {
					elem1.style.display = "none";
				}
			}
		}
		var elem2 = document.getElementById("layoutBody");
		if (elem2) {
			if (elem2.style) {
				if (elem2.style.display != null) {
					elem2.style.display = "";
				}
			}
		}
		var elem3 = document.getElementById("layoutContent");
		if (elem3) {
			if (elem3.style) {
				if (elem3.style.display != null) {
					elem3.style.display = "";
				}
			}
		}
		var elem4 = document.getElementById("layoutSubContent");
		if (elem4) {
			if (elem4.style) {
				if (elem4.style.display != null) {
					//elem4.style.display = "";
				}
			}
		}
		var elem5 = document.getElementById("layoutDescription");
		if (elem5) {
			if (elem5.style) {
				if (elem5.style.display != null) {
					elem5.style.display = "";
				}
			}
		}
		var elem6 = document.getElementById("layoutProgressBar");
		if (elem6) {
			if (elem6.style) {
				if (elem6.style.display != null) {
					elem6.style.display = "";
				}
			}
		}
	}
	
	//common.log("page revealed");
	//For plupload html4, need to do explicit refresh only after the elements are unhidden.
	vitas_fileuploader.refreshAllUploaders();
}


function checkRevealPage() {
		showElement("main", "layoutContent");
		hideElement("main", "layoutSubContent");
		//parent.frames['timer'].focus();	
		
		revealPage();
		if (self.refreshImage) { self.refreshImage(); }
		if (self.reset) { reset(); }
		if (self.onLoadHandler) { onLoadHandler(); }
		if (getFrame("main")) { getFrame("main").focus(); }
		if (self.focusControlOnPage) { focusControlOnPage(); }
}

function generateRandom() {

	var d=new Date();
	var	random = Math.floor(Math.random()*10000000001);
	
	return d.getTime() + "." + random;
}

function disableAutoComplete() {
	//console.log("disableAutoComplete");
	//console.log("check for - checkerID");
	
	for (var x=0; x<checkersIDList.length; x++) {
		var checkerID = checkersIDList[x];
	
		var checkers = document.getElementsByName(checkerID);
		for (var i=0; i<checkers.length; i++) {
			var check = checkers[i];
			check.setAttribute("autocomplete","off");
			//console.log(check);
		}	
	}
}

var cookie_name_font_size = "siteFontSize";

function adjustSize(indicator){
	
	var rule = getCSSRule("body");
	
	//try to get fontsize from cookie if exist
	var currentSize = get_cookie(cookie_name_font_size);
	
	if (currentSize == null) {
		currentSize = rule.style.fontSize;	
	}
	currentSize = parseFloat(currentSize);
	
	if (indicator == '+'){
		if (currentSize <= 120) {
			currentSize = currentSize + 12.5;
			}
	}
	else {
		if (currentSize > 62.5) {
			currentSize = currentSize - 12.5;
		}
	}
	
	//store fontSize into cookie
	rule.style.fontSize = currentSize + "%";
	set_cookie(cookie_name_font_size, rule.style.fontSize, 360); //cannot store % in cookie.
}

function getCSSRule(selectorText) {

      var i, j, noRules, rules, rule,
      // the regex is used for case insensitive string comparison
      regex = new RegExp("^" + selectorText+"$", "i"),
      sheets = document.styleSheets,
      noSheets = sheets.length;

      for (i = 0; i < noSheets; i++) {
        rules = (sheets[i].rules || sheets[i].cssRules);
        noRules = rules.length;
		for (j = 0; j < noRules; j++) {
          if (regex.test((rule = rules[j]).selectorText)) {
            return rule;
          }
        }
      }
}

function set_cookie ( cookie_name, cookie_value,
    lifespan_in_days, valid_domain )
{
    // http://www.thesitewizard.com/javascripts/cookies.shtml
    var domain_string = valid_domain ?
                       ("; domain=" + valid_domain) : '' ;
    document.cookie = cookie_name +
                       "=" + encodeURIComponent( cookie_value ) +
                       "; max-age=" + 60 * 60 *
                       24 * lifespan_in_days +
                       "; path=/" + domain_string ;
}
function get_cookie ( c_name )
{
   	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1)
  	{
  		c_start = c_value.indexOf(c_name + "=");
  	}
	if (c_start == -1)
  	{
  		c_value = null;
  	}
	else
  	{
  		c_start = c_value.indexOf("=", c_start) + 1;
  		var c_end = c_value.indexOf(";", c_start);
  		if (c_end == -1)
  		{
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function loadSiteFontSizeFromCookie() {
	var currentSize = get_cookie(cookie_name_font_size);
	
	if (currentSize != null) {
		var rule = getCSSRule("body");
		rule.style.fontSize = currentSize;
	}
}


function docLoad() {

	

	if (top.vrlWindowID == null || top.vrlWindowID == 'undefined') {
		//do nothing. for change password page which do not have frames.
	}
	else {
		//var windowID = top.vrlWindowID.getWindowID();
		//alert("windowID = " + windowName);
		

	
		forms =	document.getElementsByTagName("form");
		for (var i=0; i<forms.length; i++) {
			form = forms[i];
	
			form._submit = form.submit;
			form.submit = function() {
				var random = generateRandom();
				var randElement = document.createElement("input");
				randElement.setAttribute("type","hidden");
				randElement.setAttribute("id","randomID");
				randElement.setAttribute("name","random");
				randElement.setAttribute("value",random);		
				this.appendChild(randElement);

				var windowID = top.vrlWindowID.getWindowID();
				var element = document.createElement("input");
				element.setAttribute("type","hidden");
				element.setAttribute("name","windowID");
				element.setAttribute("value",windowID);		
				this.appendChild(element);
				
				this._submit();
				
				try {
					this.removeChild(this.lastChild); // remove random element
					this.removeChild(this.lastChild); // remove windowID element
				} catch (err){
				}
			};
			
		}
	
		
		/*
		$( forms ).submit( function( event) { 
			//alert('Your form is not going to be submitted. If you see this and it is not due to Enter key pressed, please let Brian/Harry know. Take down all the screenshots on how you arrive this scenario.');
			return false; 
		} ); //to disable form submission when Enter is hit
		*/
		
		disableAutoComplete();

	}
	
	
	handleEn2SpecialAccount();
	
	
	hideElement("banner", "theMenuBlock");
	setTimeout("checkRevealPage()", 0);

	


	
		loadSiteFontSizeFromCookie();
	


}

function handleEn2SpecialAccount(){

	var isEn2SpecialAcc = '';
	
	
	if(isEn2SpecialAcc == 'Y')
	{
		if (window.name == null || window.name == 'undefined' || window.name == '') //not clicked from Menu
		{
			var message="Only one window for transaction is allowed at one time.";

		    if (isFirefox()) {
				message = message + " Please close this window.";
			}
			else {
				message = message + " This new window will be closed.";
			}
			alert(message);
			window.document.body.style.visibility = "hidden";
			close_window_without_prompt();
		}
	}
	
}

function docUnload() {
	if (self.onUnloadHandler) { onUnloadHandler(); }
}


function encryptPIN(id) {

	var element = document.getElementsByName(id);
	for (i = 0; i < element.length; i++) {
		if (element[i].value != null) {		
			var encryptStrByScript = do_encrypt("008068f9f3dbb7a1e9a01197c51dc9ca2a2ea9104c160f25b8a98038112aeffb7ece105313e1e432043fa3dd35a47e3ddbe4347118f0d8a4c937d35381e9fb043ce983ba45d081307971b1237b2f3b8242b4f2ddb7793e97089db4a72218965716c05133cc628dedd03667ef6392d4c5a6baf1bd44147878b58a5ba6303559590d","010001",element[i].value);
			element[i].value = encryptStrByScript;		
		}
	}
}

var vitas_fileuploader;
var vitas_deleted_file_array = new Array();

$(document).ready(function(){
	common.log("ready");
	
	var hide = true;
	
	$("#layoutDescription td").each(function(index) {
		var content = $.trim($(this).html());
		if (content.length > 0) {
			hide = false;
			return false; // break out of each loop
		}
	});
	
	if (hide) {
		$("#layoutDescription").hide();
	}
	
	vitas_fileuploader = new vitas.fileupload( { flash_swf_url : '/vrl/scripts/plupload/plupload.flash.swf',
												silverlight_xap_url : '/vrl/scripts/plupload/plupload.silverlight.xap',
												upload_url : 'https://vrl.lta.gov.sg/lvs/vitas/rs/upload?FUNCTION_ID=F2300002TT',
												max_file_upload_size : '50 ',
												max_file_name_length : '33'
													
												//success : success, //optional, only if you need the uploader to update you the status via callback function.
												//fail : fail //optional, only if you need the uploader to update you the status via callback function.
											 });
											 
	$("div .fileuploadcontainer").each( function( index ) {
  		var id = $(this).attr("id");
  		common.log( index + " -> id : " + id );
  	
  		if (typeof id == "undefined" || id == null) {
  			common.log("undefined id");
  		}
  		else {
  			vitas_fileuploader.initContainer(id);
  		}
	});
	
	$( document ).ajaxStart(function() {
	
		//need to check if the timer exist, in case this page is a pop-up(without parent frame)
		if (parent.frames['timer']) {
			common.log("ajax start - reset timer");
			parent.frames['timer'].resetTime();
		}
	});	

	function URLRepo() {
		var pmUrls, chUrls, mappingIdxs;
		
		pmUrls = [
			"http://ask.lta.gov.sg/cfp/pages/Themes/LTA/displayresult.aspx?MesId=2287964"
		];
		chUrls = [
			"http://ask.lta.gov.sg/cfp/pages/Themes/LTA/displayresult.aspx?MesId=2287842"
		];
		mappingIdxs = {
			"F0101001TT": [ 0, 0 ]
			,"F0202001TT": [ 0, 0 ]
			,"F0203001TT": [ 0, 0 ]
			,"F0203004TT": [ 0, 0 ]
			,"F0203005TT": [ 0, 0 ]
			,"F0203006TT": [ 0, 0 ]
			,"F0203008TT": [ 0, 0 ]
			,"F0203015TT": [ 0, 0 ]
			,"F0204001TT": [ 0, 0 ]
			,"F0204006TT": [ 0, 0 ]
			,"F0302002TT": [ 0, 0 ]
			,"F0302003TT": [ 0, 0 ]
			,"F0302005TT": [ 0, 0 ] 
			,"F0303003TT": [ 0, 0 ]
			,"F0303004TT": [ 0, 0 ]
			,"F0302005TT": [ 0, 0 ]
			,"F0401013TT": [ 0, 0 ]
			,"F1601002TT": [ 0, 0 ] 
			,"F0501002TT": [ 0, 0 ]
			,"F0501009TT": [ 0, 0 ]
			,"F1303002TT": [ 0, 0 ]
			,"F1601002TT": [ 0, 0 ] 
			,"F1603002TT": [ 0, 0 ]
			,"F1603003TT": [ 0, 0 ]
			,"F1603004TT": [ 0, 0 ]
			,"F1701001TT": [ 0, 0 ]
			,"F1701003TT": [ 0, 0 ]
			,"F1701012TT": [ 0, 0 ]
			,"F0701004TT": [ 0, 0 ]
			,"F0701005TT": [ 0, 0 ]
			,"F0706802TT": [ 0, 0 ]
			,"F0706803TT": [ 0, 0 ]
			,"F0705006ET": [ 0, 0 ]
			,"F0901001TT": [ 0, 0 ]
			,"F0901002TT": [ 0, 0 ]
		};
		
		this.getPaymentLimitUrl = function( functionId ) {
			var urlIdx = ( mappingIdxs[ functionId ] || [] )[ 0 ];
			if ( typeof urlIdx !== "undefined" ) { 
				return pmUrls[ urlIdx ]; 
			}
			else { 
				console.log( "No index mapping for " + functionId ); 
				return null; 
			}
		};
		
		this.getClickHereUrl = function( functionId ) {
			var urlIdx = ( mappingIdxs[ functionId ] || [] )[ 1 ];
			if ( typeof urlIdx !== "undefined" ) { 
				return chUrls[ urlIdx ]; 
			}
			else { 
				console.log( "No index mapping for " + functionId ); 
				return null; 
			}
		};
	}
	
	(function() {
		var repo = new URLRepo(),
			fId = 'F0501015ET';
		$( "a.dynamic-url" ).on( "click", function() {
			var this$ = $( this ), url;
			
			if ( this$.attr( "href" ) === "#" ) {
				if ( this$.hasClass( "dynamic-url-pm" ) ) {
					url = repo.getPaymentLimitUrl( fId );
				}
				else if ( this$.hasClass( "dynamic-url-ch" ) ) {
					url = repo.getClickHereUrl( fId );
				}
				
				this$.attr( "href", url );
			}
		});
	})();
});


</script>
</head>

<body bgcolor="#ffffff" text="#000000" link="#023264" alink="#023264" vlink="#023264" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="docLoad();" onunload="docUnload();" onclick="resetMouseTrack();" oncontextmenu="return true;">

<div id="layoutPlsWait" class="messageTxt12pt" style="text-align:center; color:black;">
	<br>
	Please wait while we are processing your request...
</div>

<div id="layoutHeader" align="center">








<div id="timeOutMsg" style="display:none">
<p align="center">

	<table width="100%" class="LoginTxt">
	<tbody><tr>
	<td width="30%">&nbsp;</td>
	<td width="30%" align="left">
		<img src="./Transfer Fee Enquiry_files/img_thankqOM.jpg" width="277" height="55">
	</td>
	<td width="40%" class="footerLink" valign="top">
		<img src="./Transfer Fee Enquiry_files/img_veh_out.jpg" width="200"><br>
	</td>
	</tr>
	<tr>
	<td colspan="3" align="center">
		<table align="left" width="100%">
			<tbody><tr><td class="messageTxt12pt" align="center">
				<font style="color:red">
					You have been logged out as your session has been inactive.
				</font>
			</td></tr>
			<!-- <tr><td class="microgap">&nbsp;</td></tr> -->
			<tr><td class="messageTxt12pt" align="center">
				Your last login date/time was 
			2015-05-19 11:15:16.318.
			</td></tr>
			<!-- <tr><td class="microgap">&nbsp;</td></tr> -->
			<tr><td class="messageTxt12pt" align="center">
				To return to One Motoring, please click 
				<a target="_parent" href="http://localhost/pkmslogout.form"><b>here</b></a>.
				</td></tr>
			<!-- <tr><td class="microgap">&nbsp;</td></tr> -->
			<tr><td class="messageTxt12pt" align="center">
				For security reasons, please <a href="http://localhost/dohinsurance/public/scrap#" onclick="javascript:window.open(&#39;/vrl/html/cache.html&#39;,&#39;cache&#39;, &#39;width=500,height=310,resizable=yes,scrollbars=yes&#39;)">
				<b>CLEAR YOUR CACHE</b></a> after each session.
			</td></tr>
		</tbody></table>
	</td>
	</tr>
	</tbody></table>

</p>
</div>
</div>

<div id="layoutMenu" align="center">



</div>

<div id="layoutTimer" align="center" style="display:none">
<style>
.dialog{
	font-family:Tahoma; font-size:12pt; text-align:center
}
</style>
<table border="0" cellspacing="0" cellpadding="1" align="center" width="300" bgcolor="blue">
<tbody><tr>
<td>

	<table border="0" cellspacing="2" cellpadding="2" width="450" align="center" valign="center">
	<tbody><tr style="background-color:blue;">
	<th id="winheader" class="dialog" style="text-align:left; color:#FFFFFF;">
		<script>
			var agt = navigator.userAgent.toLowerCase();
			var isIE = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
			if (isIE) {
				document.writeln("Microsoft Internet Explorer"); 
			} else {
				document.writeln("[JavaScript Application]"); 
			}
		</script>[JavaScript Application]

	</th>
	<th class="dialog" style="text-align: right;" onclick="reset();">
		<img src="./Transfer Fee Enquiry_files/alert_close.png" width="17" height="17">
	</th>
	</tr>
	<tr>
	<td class="dialog" style="text-align:center" colspan="2">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center" valign="center" class="dialog" bgcolor="#ECE9D8">
			<tbody><tr>
				<td class="dialog" style="width:20%; text-align:center">
					<br>
					<img src="./Transfer Fee Enquiry_files/alert_logo.png" border="0/">
					<br>
				</td>
				<td class="dialog" style="width: 90%">
					<br>
					<script>document.writeln(preTimeoutMsg);</script>
					<br>
				</td>
			</tr>
	
			<tr>
				<td class="dialog" colspan="2">
					<div id="alertDisplay" name="alertDisplay" style="color:red;text-align:center"></div>
				</td>
			</tr>
	
			<tr>
				<td class="dialog" colspan="2">
					<input name="yes" type="button" value="     OK     " onclick="reset();">
					<input name="no" type="button" value=" Cancel " onclick="logout();">
					<br>
					<br>
				</td>
			</tr>
		</tbody></table>
	</td>
	</tr>
	</tbody></table>

	
</td>
</tr>
</tbody></table>

</div>

<div id="layoutDescription" align="center">
 
<table width="95%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr> 
		<td width="100%" class="topText" align="right" valign="top">










<div id="textAdjuster">
	Text size
	<a style="text-decoration: none; color: #666;" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript: adjustSize(&#39;+&#39;); return false;">+</a> &nbsp;
	<a style="text-decoration: none; color: #666;" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript: adjustSize(&#39;-&#39;); return false;">-</a>
</div>






</td>
	</tr>
</tbody></table>
</div>

<div id="layoutProgressBar" align="center">


















</div>

<div id="layoutContent" style="/* display:none */">






















<script language="JavaScript" src="./Transfer Fee Enquiry_files/formObjCommons.js"></script>
<script language="JavaScript" src="./Transfer Fee Enquiry_files/vrlCommons(1).js"></script>
<script language="JavaScript">
var strColon = ':';
var strHyphen = '-&nbsp;';

	function returnToFirstPage(form){
		form.dispatch.value = "ok";
		redirectRequest("enquireTransferFeeProxy" , "F0501015ET");
	}
	
	function printForm(form) {
		window.print();
	}
	
</script>

<form name="enquireTransferFeeForm" method="post" action="http://localhost/lta/vrl/action/enquireTransferFeeDetailsProxy">
<input type="hidden" name="dispatch" value="">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tbody><tr>
		<td align="left" valign="top" width="95%">&nbsp;</td>
	</tr>

	
		<tr>
			<td align="center" valign="top">
				<table width="95%" border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td class="Head_Bk14pt" colspan="2">Enquire Transfer Fee</td>
					</tr>
					<tr>
						<td class="TableTopYellowWTxt" colspan="2">Vehicle Details</td>
					</tr>
					















	<tr class="TableY1WTxt">
		<td height="25" width="20%">Vehicle No.<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				SFY1212X
			
		</td>
	</tr>
	<tr class="TableB1WTxt">
		<td height="25" width="20%">Vehicle Type<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				P11 - Passenger Station Wagon/Jeep/Land Rover
			
		</td>
	</tr>
	<tr class="TableY1WTxt">
		<td height="25" width="20%">Vehicle Attachment 1<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				With Sun Roof
			
		</td>
	</tr>
	<tr class="TableB1WTxt">
		<td height="25" width="20%">Vehicle Scheme<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				Normal
			
						
		</td>
	</tr>
	<tr class="TableY1WTxt">
		<td height="25" width="20%">Vehicle Make<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
			
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
						MITSUBISHI
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
					
				
			
		</td>
	</tr>
	<tr class="TableB1WTxt">
		<td height="25" width="20%">Vehicle Model<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				AIRTREKTB SR
			
		</td>
	</tr>

	
		<tr class="TableY1WTxt">
			<td height="25" width="20%">Chassis No.<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">JMYLYCU2W5U000577</td>
		</tr>
	

	
	<!-- SR2012/033 start -->
	
		<tr class="TableB1WTxt">
        	<td height="25" width="20%">Propellant:</td>
   			<td height="25" width="80%">Petrol</td>
		</tr>	
	
	<!-- end SR2012/033 -->
	
	
		<tr class="TableY1WTxt">
			<td height="25" width="20%">Engine No.<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">4G63LH6677</td>
		</tr>	
	

	
	
	
		<tr class="TableB1WTxt">
			<td height="25" width="20%">Engine Capacity<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">
				
				
					1997 cc
				
			</td>
		</tr>	
	

	

		<tr class="TableY1WTxt">
        	<td height="25" width="20%">Maximum Power Output:</td>
  	   			<td height="25" width="80%">
	        	
					177.0&nbsp;kW
					&nbsp;(237&nbsp;bhp)	    	      			
   				
	        	   				
   				</td>
		</tr>	
	
	<tr class="TableB1WTxt">
		<td height="25" width="20%">Maximum Laden Weight:</td>
		<td height="25" width="80%">
			
				
					
						
						
							-							
						
											
				
				
			
		</td>
	</tr>
	
	<tr class="TableY1WTxt">
		<td height="25" width="20%">Unladen Weight:</td>
		<td height="25" width="80%">
			
				
					
						
						
							-							
						
											
				
				
			
		</td>
	</tr>	

	<tr class="TableB1WTxt">
		<td height="25" width="20%">Year Of Manufacture:</td>
		<td height="25" width="80%">2005</td>
	</tr>

	<tr class="TableY1WTxt">
		<td height="25" width="20%">Original Registration Date:</td>
		<td height="25" width="80%">24 Aug 2005</td>
	</tr>

	<tr class="TableB1WTxt">
		<td height="25" width="20%">Lifespan Expiry Date<script language="JavaScript">document.write(strColon)</script>:</td>
		<td height="25" width="80%">
			
				
				
					-
				
					
		</td>
	</tr>

	
			
			<tr class="TableY1WTxt">
				<td height="25" width="20%">COE Category<script language="JavaScript">document.write(strColon)</script>:</td>
				<td height="25" width="80%">B - Car (1601cc &amp; above)</td>
			</tr>
		
		
			<tr class="TableB1WTxt">
				<td height="25" width="20%">Quota Premium<script language="JavaScript">document.write(strColon)</script>:</td>
				<td height="25" width="80%">$16,303.00</td>
			</tr>
			<tr class="TableY1WTxt">
				<td height="25" width="20%">COE Expiry Date<script language="JavaScript">document.write(strColon)</script>:</td>
				<td height="25" width="80%">23 Aug 2015</td>
			</tr>			
		
		

		
		<tr class="TableB1WTxt">
			<td height="25" width="20%">Road Tax Expiry Date<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">23 Aug 2015</td>
		</tr>	
	
	
	
	
		
		<tr class="TableY1WTxt">
			<td height="25" width="20%">PARF Eligibility Expiry Date<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">23 Aug 2015</td>
		</tr>	
	
	
		
		<tr class="TableB1WTxt">
			<td height="25" width="20%">Inspection Due Date<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">23 Aug 2016</td>
		</tr>	
	
	
		
		<tr class="TableY1WTxt">
			<td height="25" width="20%">Intended Transfer Date<script language="JavaScript">document.write(strColon)</script>:</td>
			<td height="25" width="80%">01 Aug 2015</td>
		</tr>
		
	<!--  Begin of CEV Rebate Details  //Added for SR 2012/009 -->  
	  				
	 
		<tr class="TableB1WTxt">
		       <td width="20%" nowrap="" height="25">
		             CO2 Emission:
		       </td>
		       <td colspan="3" height="25">
					-
			   </td>
		</tr>  
	
		  				           
	<!-- End of CEV rebate details -->
					
	
		<tr class="TableY1WTxt">
			
				
					
					
						
							
							
								<td height="25" width="100%" colspan="2">Late renewal fee(s) will be imposed if road tax / lay up has expired. Please use<a href="http://localhost/dohinsurance/public/pubfunc?ID=EnquireRoadTaxPayable" class="table_txtBk10ptLINK" target="window">Enquire Road Tax Payable</a>&nbsp;for fee(s) payable.</td>							
							
						
					
				
				
			
		</tr>
		

	<tr class="TableB1WTxt">
		<td height="25" width="100%" colspan="2">Road tax, including Over Payment (if any), of a vehicle will follow the vehicle to the new registered owner when its ownership is being transferred.</td>
	</tr>

				</tbody></table>
			</td>
		</tr>

		
			
		

		
			
		
				
		
			
		

		
			
		
				
		
			
		

		
			
		
				
		
			
		

		
			
		
		
		
			
		

		
			
		
				
		
				
		
				
						

		

		
			<tr>
				<td align="center" valign="top">
					<table width="95%" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="TableTopYellowWTxt" colspan="4">
								
								
								<!-- modified for SR2012/033 -->
								
									Amount Payable
								
							</td>
						</tr>
						
						<tr class="TableY1WTxt">
							<td width="20%" height="25">&nbsp;</td>
							<td width="30%" height="25" align="right"><b>Amount Before GST<br>(S$)</b></td>
							<td width="20%" height="25" align="right"><b>GST Amount<br>(S$)</b></td>
							<td width="30%" height="25" align="right"><b>Amount After GST<br>(S$)</b></td>
						</tr>
						
							<tr class="TableB1WTxt">
								<td height="25" width="20%" align="left">Transfer Fee<script language="JavaScript">document.write(strColon)</script>:</td>
								<td height="25" width="30%" align="right">11.00</td>
								<td height="25" width="20%" align="right">
								
									
									
										<script language="JavaScript">document.write(strHyphen)</script>-&nbsp;											
									
								
								</td>
								<td height="25" width="30%" align="right">
									11.00
								</td>
							</tr>
						
						
						
						

						
						
						<tr class="TableY1WTxt">
							<td height="25" width="20%" align="left"><strong>Total Amount Payable<script language="JavaScript">document.write(strColon)</script>:</strong></td>
							<td height="25" width="30%" align="right"></td>
							<td height="25" width="20%" align="right"></td>
							<td height="25" width="30%" align="right">
								
								<!-- SR2012/033 -->
								
								
									
						 				<strong>11.00</strong>
						 			
															 			
																
							</td>
						</tr>
						
					</tbody></table>
				</td>
			</tr>
		

		

		

		

		

		
		<!-- VP0101A/2012/033 -->
		
			<tr>
				<td align="center" valign="top">
					<table width="95%" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td class="TableTopYellowWTxt" colspan="1">Message</td>
						</tr>
						
						

						

						

						

						

						
						<!-- SR2012/033 -->
						
						
						<!-- START: SR2015/023 -->
						
							<tr class="TableY1WTxt">
								<td height="25"><b><font color="#FF0000">The Government will grant a one-year road tax rebate for petrol and petrol-hybrid vehicles from 1 Aug 2015. The current enquiry result does not include the road tax rebate. Please refer to the <a href="http://www.lta.gov.sg/apps/news/page.aspx?c=2&id=b58ca1ea-976f-4f37-9e7f-3418666d8525" target="_blank"><font color="#FF0000">Press Release</font></a> for more information.</font></b></td>
							</tr>						
						
						<!-- END: SR2015/023 -->
						
					</tbody></table>
				</td>
			</tr>
		
	

	









	

	<tr>
		<td align="center" valign="top">
		<table width="95%" border="0" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="BodyTxtBlk" height="25">You may print this page for reference.</td>
			</tr>
		</tbody></table>
		</td>
	</tr>
	<tr>
		<td align="center" width="98%">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
		<div align="center">
		<input type="button" name="OK" value="OK" onclick="returnToFirstPage(this.form)"> 
		<input type="button" name="Print" value="Print" onclick="printForm(this.form)">
		</div>
		</td>
	</tr>
</tbody></table>
</form>
</div>

<div id="layoutFooter" align="center">



<div id="footerPara" style="text-align:center">



<table cellspacing="0" cellpadding="0" border="0" class="footer" style="text-align:center" align="center">
<tbody><tr>
<td>
	<img src="./Transfer Fee Enquiry_files/ltalogo2.gif" alt="LTA logo">
</td>
</tr>

<tr>
<td class="footerTxt">
	Please read through the Privacy Statement, Terms of Use and Disclaimer.
</td>
</tr>

<tr>
<td class="footerTxt">
	Please do not use the <b>Back</b> or <b>Forward</b> 
	buttons on your browser as this may alter the results of the transactions.
</td>
</tr>

<tr class="footerTxtFixed">
<td>Best viewed with IE 6.0 SP3 and above. 1024 X 768 resolution</td>
</tr>
<tr class="footerTxtFixed">
<td>
	Copyright © <script>document.write(new Date().getFullYear());</script>2015 LTA
	&nbsp;I&nbsp;
	<a class="footerLink" title="Privacy Statement" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript:window.open(&#39;/lta/vrl/html/privacyPolicy.html&#39;, &#39;_ppWin&#39;, &#39;width=800,height=530,scrollbars=1,resizable=1,toolbar=0,location=0,menubar=0,status=0,directories=0&#39; ).focus();return false">Privacy Statement</a>
	&nbsp;I&nbsp;
	<a class="footerLink" title="Terms of Use" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript:window.open(&#39;/lta/vrl/html/conditionsOfAccess.html&#39;, &#39;_ppWin&#39;, &#39;width=800,height=530,scrollbars=1,resizable=1,toolbar=0,location=0,menubar=0,status=0,directories=0&#39; ).focus();return false">Terms of Use</a>
	&nbsp;I&nbsp;
	<a class="footerLink" title="Disclaimer" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript:window.open(&#39;/lta/vrl/html/disclaimer.html&#39;, &#39;_ppWin&#39;, &#39;width=800,height=530,scrollbars=1,resizable=1,toolbar=0,location=0,menubar=0,status=0,directories=0&#39; ).focus();return false">Disclaimer</a>
	&nbsp;I&nbsp;
	<a class="footerLink" title="Rate the Website" href="http://localhost/dohinsurance/public/scrap#" onclick="javascript:window.open(&#39;http://app.lta.gov.sg/feedback/PublicSurvy.aspx?SurveyDetlNum=S201400005&#39;, &#39;_ppWin&#39;, &#39;width=800,height=530,scrollbars=1,resizable=1,toolbar=0,location=0,menubar=0,status=0,directories=0&#39; ).focus();return false">Rate the Website</a>
	 I <a href="https://www.lta.gov.sg/feedback/PublicSurvy.aspx?SurveyDetlNum=S201400011" target="_blank">Rate this e-Service</a>
</td>
</tr>
</tbody></table>

</div>
<script>
if (self.isWelcome) {
	document.getElementById("footerPara").style.display = "none";
}
</script>
</div>







</body></html>