<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Goutte\Client;
use Request;

class ScrapController extends Controller {

	/**
	*	The Main function of Scraping
	*
	**/
	public function get_captcha(){
		$useragent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36';
		$url = 'http://poster.de-captcher.com/';
		$path = public_path().'\images\captcha.png';
		$content = "image/png";
		$name = "captcha.png";
		$image = curl_file_create($path,$content,$name);
		$data = array(
				 'function'=>'picture2',
				 'username'=>'vhiearch',
				 'password'=>'vhiearch',
				 'pict_to'=>'0',
				 'pict_type'=>'0',
				 'pict'=>$image,
				 'submit'=>'Send',
				);
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_HEADER, 1);
		//curl_setopt($ch,CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,300);
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
	    $postResult = curl_exec($ch);

	    $pieces = explode("|",$postResult);

	    return $pieces[5];
	}

	public function grab_image($url){
		$saveto = public_path(). '\images\captcha.png';
		$cookie = public_path(). '\cookie\cookie.txt';
		$ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	  	// curl_setopt($ch, CURLOPT_COOKIE, $str);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	    $raw=curl_exec($ch);
	    curl_close ($ch);
	    if(file_exists($saveto)){
	        unlink($saveto);
	    }
	    $fp = fopen($saveto,'x');
	    fwrite($fp, $raw);
	    fclose($fp);
	}

	public function post_curl($url,$data){
		$cookie = public_path().'\cookie\cookie.txt';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$resp = curl_exec($ch);

		if($resp == false){
			echo "Error Number:".curl_errno($ch)."<br>";
       		echo "Error String:".curl_error($ch);
		}
		curl_close($ch);
		return $resp;
	}

	public function get(){
		return view('guest.scrap');
	}

	public function post(){

		// Save Captcha Image
		$url_image = "https://vrl.lta.gov.sg/lta/vrl/action/captchaImg?FUNCTION_ID=F0501015ET";
		$this->grab_image($url_image);

		//Solving Captcha
		$getcaptcha = $this->get_captcha();

		if(!$getcaptcha){
			return view('errors.getcaptcha');
		}else{

			$url = 'https://vrl.lta.gov.sg/lta/vrl/action/enquireTransferFeeDetailsProxy?FUNCTION_ID=F0501015ET';

			$getdata = array(
				'vehicleNo'=>Request::input('vehicleNo'), 
				'transferDate'=>'01082015', 
				'captchaResponse'=>$getcaptcha, 
				'dispatch'=>'submit'
			);

			//echo $this->post_curl($url,$getdata);
			$client = new Client();
			$cookie = public_path().'\cookie\cookie.txt';
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 60);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYPEER, FALSE);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_FOLLOWLOCATION, TRUE);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_COOKIEFILE, $cookie);
			$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_COOKIEJAR, $cookie);
			
			$crawler = $client->request('POST',$url,$getdata);

			$crawler->filter('html div#layoutContent table tr td[width="80%"]')->each(function ($node){
				file_put_contents(public_path().'\content\string.txt', preg_replace('/\s+/', ' ',$node->text()),FILE_APPEND);
			});

			$file = public_path().'\content\string.txt';

			$fopen = fopen($file,'r');

			$fread = fread($fopen,filesize($file));

			fclose($fopen);

			$split = explode(' ',$fread);

			unlink($file);

			return view('guest.scrapresult')->with('content',$split);
		}
	}
}

