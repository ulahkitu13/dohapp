<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DohApp</title>
	<link rel="stylesheet" href="{{ asset('css/all.css') }}">
	<script src="{{ asset('js/all.js') }}"></script>
</head>
<body style="margin-top: 10%">
		@yield('testindex')
</body>
</html>