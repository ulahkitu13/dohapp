@extends('test.header')

@section('testindex')
<div class="ui two column centered grid">
	<div class="column">
		{!! Form::open(['url'=>'http://poster.de-captcher.com/','method'=>'post','class'=>'ui fluid form segment','enctype'=>'multipart/form-data']) !!}
			<h2 class="ui dividing header">OCR Captcha Solver using De-Captcher API</h2>
			<div class="field">
				{!! Form::hidden('function', 'picture2') !!}
			</div>
			<div class="field">
				{!! Form::text('username', 'vhiearch') !!}
			</div>
			<div class="field">
				{!! Form::text('password', 'vhiearch') !!}
			</div>
			<div class="field">
				{!! Form::text('pict_to', '0') !!}
			</div>
			<div class="field">
				{!! Form::text('pict_type', '0') !!}
			</div>
			<div class="field">
				{!! Form::file('pict') !!}
			</div>
			{!! Form::submit('send', array('class'=>'ui green button')) !!}
		{!! Form::close() !!}
	</div>
</div>
@endsection