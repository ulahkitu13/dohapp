@extends('guest.header')

@section('scrapform')
<div class="ui two column centered grid">
	<div class="column">
		{!! Form::open(['action'=>'ScrapController@post','method'=>'post','class'=>'ui fluid form segment','enctype'=>'multipart/form-data']) !!}
			<h2 class="ui dividing header">Search Vehicle Details</h2>
			<div class="field">
				<label>Vehicle No</label>
				{!! Form::text('vehicleNo', null, array('placeholder'=>'e.g: SFY1212X','onBlur'=>'this.value=this.value.toUpperCase()','style'=>'text-transform: uppercase','required'=>true)) !!}
			</div>
			{!! Form::submit('search', array('class'=>'ui green button')) !!}
		{!! Form::close() !!}
	</div>
</div>
@endsection
