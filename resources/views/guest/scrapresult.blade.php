@extends('guest.header')

@section('scrapresult')
<div class="ui two column centered grid">
	<div class="column">
		<div class="ui piled segment">
			<h2 class="ui header">Vehicle Detail Search Result</h2>
				<p>
					<b>Vehicle No :</b> {{ $content[1] }}
				</p>
				<p>
					<b>Vehicle Make :</b> {{ $content[16] }}
				</p>
				<p>
					<b>Vehicle Model :</b> {{ $content[18] }} {{ $content[19] }}
				</p>
				<p>
					<b>Year of Manufacture :</b> {{ substr($content[30],0,4) }}
				</p>
				<p>
					<b>Originial Registration Date :</b> {{ substr($content[30],4) }} {{ $content[31] }} {{ $content[32] }}
				</p>
		</div>
	</div>
</div>
@endsection